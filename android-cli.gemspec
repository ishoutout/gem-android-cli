Gem::Specification.new do |s|
  s.name        = 'android-cli'
  s.version     = '0.0.4'
  s.date        = '2014-07-08'
  s.summary     = "A ruby wrapper for android"
  s.description = "Runs android commands from ruby such as `android update project`"
  s.authors     = ["Thys Ballard"]
  s.email       = 'tballard@ishoutout.net'
  s.files       = ["lib/android-cli.rb"]
  s.homepage    = 'https://bitbucket.org/ishoutout/gem-android-cli'
  s.license     = 'MIT'
end
