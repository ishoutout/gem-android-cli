# Finds Android SDK ID
#
# @author Thys Ballard Here <tballard@ishoutout.net>

class AndroidCLI

  @@sdkPath = nil

  def self.setSDKPath(path)
    @@sdkPath = path
  end

  def self.getSDKPath
    return `which android` if @@sdkPath.nil?
    @@sdkPath
  end

  # Determines if android is installed
  # @return BOOLEAN
  def self.installed?
    return system("which #{getSDKPath}/android")
  end

  # Runs`android list targets`
  #
  # @return STDOUT `android list targets`
  def self.listTargets
    `#{getSDKPath}/android list targets`
  end

  # Determines the Android SDK ID
  #
  # Searches the STDOUT of `android list targets`
  # for specified Android target human-readable string
  #
  # @param [String] target human-readable string
  # @return [Integer, nil] The android target id or nil if none could be found
  def self.searchTargets(targetString)

    matches = /id: ([0-9]+) or "#{targetString}"/.match(AndroidCLI.listTargets)

    return nil if matches.nil?

    return matches.captures[0].to_i
  end

  # Runs `android update project`
  #
  # @param [Integer] target target ID from android list targets
  # @return [Boolean] did it update the project successfully
  def self.updateProject(targetId)
    system("#{getSDKPath}/android update project -p . --subprojects  --target #{targetId}")
  end

  # Updates a project based in a human readable target
  #
  # @param [String] target target string from android list targets
  # @return [Boolean] did it update the project successfully
  def self.updateProjectFromString(targetStr)
    targetId = AndroidCLI.searchTargets(targetStr)

    return false if targetId.nil?

    return AndroidCLI.updateProject(targetId)

  end

end
