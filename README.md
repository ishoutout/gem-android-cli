#Welcome to Android CLI!

[![Gem Version](https://badge.fury.io/rb/android-cli.svg)](http://badge.fury.io/rb/android-cli)

##A ruby wrapper for Android commands

Runs android commands from ruby such as `android update project`

```ruby
require "android-cli"

AndroidCLI.updateProject(12)
AndroidCLI.updateProjectFromString("Google Inc.:Google APIs:19").nil?

```

##Installation
It's as simple as 1, 2, 3.  Just install using `gem install android-cli`  But wait there is more, make sure that Ruby is installed.
