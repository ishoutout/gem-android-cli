require_relative "../lib/android-cli"

ID = 14
API_name = "Google Inc.:Google APIs:19"

describe "AndroidCLI", "#installed?" do
  it "should return true if Android is installed" do
    expect(AndroidCLI.installed?).to eq(true)
  end
end
describe "AndroidCLI", "#listTargets" do
  it "should list possible Android SDK targets" do
    #need to determine that STDOUT contains a list

    expect(AndroidCLI.listTargets).to match(/id: ([0-9]+) or "#{API_name}"/)
  end
end
describe "AndroidCLI", "#searchTargets" do
  it "should return a respective ID # when a string is entered" do
    expect(AndroidCLI.searchTargets(API_name)).to eq(ID)
  end
  it "should return nil when there is an unexpected input" do
    expect(AndroidCLI.searchTargets(1)).to eq(nil)
  end
end

# @TODO remember to add the last to methods relating to updating Android projects
